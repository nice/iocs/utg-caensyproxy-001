#!/usr/bin/env iocsh.bash

require caensyproxy

# Set EPICS environment variables
## Edit these as required for specific IOC requirements
## General IOC environment variables
# epicsEnvSet("ENGINEER", "Douglas Araujo <douglas.araujo@ess.eu>")

epicsEnvSet(P,              "$(P=Utg-VIP:Det-PSU-01:)")        # default prefix is "CEA"
epicsEnvSet(Dis,            "$(Dis=Ctrl)")
epicsEnvSet(CRATE_PREFIX,   "Utg-VIP:Det-PSU-01")

# CAEN crateZ
epicsEnvSet(Dev,            "PSSys")
epicsEnvSet(Idx,            "100")
epicsEnvSet(R,              "${Dis}-${Dev}-${Idx}:")
dbLoadRecords("CAEN_SY4527_crate.template", "P=${P}, R=${R}, CRATE_PREFIX="${CRATE_PREFIX}"")

## HV
# epicsEnvSet(Dev,        "HVM")
# epicsEnvSet(Idx,        "101")
# epicsEnvSet(R,          "${Dis}-${Dev}-${Idx}:")
# epicsEnvSet(HV_SLOT,    "00")
# board and its 12 channels
# dbLoadRecords("CAEN_HV_A7236D.db", "P=${P}, R=${R}, CRATE_PREFIX="${CRATE_PREFIX}", HV_SLOT="${HV_SLOT}",  EXT_TRIP="${EXT_TRIP=}"")

## HV AD7030DP
epicsEnvSet(Dev,        "HVM")
epicsEnvSet(Idx,        "102")
epicsEnvSet(R,          "${Dis}-${Dev}-${Idx}:")
epicsEnvSet(HV_SLOT,    "03")
dbLoadRecords("CAEN_HV_A7030.db", "P=${P}, R=${R}, CRATE_PREFIX="${CRATE_PREFIX}", HV_SLOT="${HV_SLOT}",  EXT_TRIP="${EXT_TRIP=}"")

## LV A2519A
epicsEnvSet(Dev,        "LVM")
epicsEnvSet(Idx,        "101")
epicsEnvSet(R,          "${Dis}-${Dev}-${Idx}:")
epicsEnvSet(LV_SLOT,    "12")
# board and its 8 channels
dbLoadRecords("CAEN_LV_A2519.db", "P=${P}, R=${R}, CRATE_PREFIX="${CRATE_PREFIX}", LV_SLOT="${LV_SLOT}", INT_TRIP="${INT_TRIP=}", EXT_TRIP="${EXT_TRIP=}"")

## LV A2552
epicsEnvSet(Dev,        "LVM")
epicsEnvSet(Idx,        "103")
epicsEnvSet(R,          "${Dis}-${Dev}-${Idx}:")
epicsEnvSet(LV_SLOT,    "06")
# board and its 8 channels
dbLoadRecords("CAEN_LV_A2519.db", "P=${P}, R=${R}, CRATE_PREFIX="${CRATE_PREFIX}", LV_SLOT="${LV_SLOT}", INT_TRIP="${INT_TRIP=}", EXT_TRIP="${EXT_TRIP=}"")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/essioc.iocsh")

# Call iocInit to start the IOC
iocInit()

